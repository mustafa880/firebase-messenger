//
//  LoginController+handlers.swift
//  Messenger
//
//  Created by Mustafa on 7/4/18.
//  Copyright © 2018 Mustafa. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func handleRegister() {
        // Show loading Indicator
        startLoading()
        guard let name = nameTextField.text, let email = emailTextField.text, let password = passwordTextField.text else {
            self.stopLoading()
            print("Form is not valid!")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { (res, err) in
            if err != nil {
                self.stopLoading()
                let alert = UIAlertController(title: "Error", message: err?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                print("Registration Failed \(err!)")
                return
            }
            
            // Successfuly Authenticated User...
            let imageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("images/\(imageName).jpg")
            
            if let profileImage = self.profileImageView.image , let uploadData = UIImageJPEGRepresentation(profileImage, 0.5) {
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    
                    storageRef.downloadURL(completion: { (url, err) in
                        guard let downloadUrl = url?.absoluteString else { return }
                        let values = ["name": name, "email": email, "profileImageUrl": downloadUrl] as [String : AnyObject]
                        self.registerUserIntoDatabaseWithValues(values: values)
                    })
                    
                }) // End of Put Data Completion Handler
            } // End of Unwrapp Image
        } // End of Create User Completion Handler
    }
    
    private func registerUserIntoDatabaseWithValues(values: [String: AnyObject]){

        let ref: DatabaseReference!
        ref = Database.database().reference()
        guard let user = Auth.auth().currentUser
            else{
                self.stopLoading()
                return
        }
        // Hide loading Indicator
        self.stopLoading()
        let usersRef = ref.child("users").child(user.uid)
        //let values = ["name": name, "email": email]
        usersRef.updateChildValues(values, withCompletionBlock: { (error, refe) in
            if error != nil {
                self.stopLoading()
                print("Update Values Failed \(error!)")
                return
            }
            self.stopLoading()
            let userObject = User();
            userObject.name = values["name"] as? String
            userObject.email = values["email"] as? String
            userObject.profileImageUrl = values["profileImageUrl"] as? String
            self.messagesController?.setupNavBarWithUser(user: userObject)
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    
    @objc func handleSelectProfileImageView() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }  
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileImageView.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Canceled Picker")
        dismiss(animated: true, completion: nil)
    }
}
