//
//  Message.swift
//  Messenger
//
//  Created by Mustafa on 8/17/18.
//  Copyright © 2018 Mustafa. All rights reserved.
//

import UIKit
import FirebaseAuth

class Message: NSObject {
    var text: String?
    var fromId: String?
    var toId: String?
    var timestamp: NSNumber?
    var imageUrl: String?
    var imageWidth: NSNumber?
    var imageHeight: NSNumber?
    
    func chatPartnerId() -> String {
        return fromId == Auth.auth().currentUser?.uid ? toId! : fromId!
    }
    
    init(dictionary: [String: AnyObject]) {
        super.init()
        fromId = dictionary["fromId"] as? String
        text = dictionary["text"] as? String
        timestamp = dictionary["timestamp"] as? NSNumber
        toId = dictionary["toId"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        imageWidth = dictionary["imageWidth"] as? NSNumber
        imageHeight = dictionary["imageHeight"] as? NSNumber
    }
}
