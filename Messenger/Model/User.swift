//
//  User.swift
//  Messenger
//
//  Created by Mustafa on 7/1/18.
//  Copyright © 2018 Mustafa. All rights reserved.
//

import UIKit

class User: NSObject {
    var id: String?
    var email: String?
    var name: String?
    var profileImageUrl: String?
}
